package com.repository.materialdesign.adapter.viewPager

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.repository.materialdesign.fragment.tabLayout.*

class ViewPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 10

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> TabLayoutSubFragmentOne()
            1 -> TabLayoutSubFragmentTwo()
            2 -> TabLayoutSubFragmentThree()
            3 -> TabLayoutFragmentFour()
            4 -> TabLayoutFragmentFive()
            5 -> TabLayoutFragmentSix()
            6 -> TabLayoutFragmentSeven()
            7 -> TabLayoutSubFragmentEight()
            8 -> TabLayoutFragmentNine()
            9 -> TabLayoutFragmentTen()
            else -> TabLayoutSubFragmentOne()
        }
    }
}