package com.repository.materialdesign.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.repository.materialdesign.databinding.FragmentSelectionControlsBinding


class SelectionControlsFragment : Fragment() {

    private var binding: FragmentSelectionControlsBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSelectionControlsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            switchId.setOnCheckedChangeListener { _, isEnabled ->
                showToastMessage("Switch enabled: $isEnabled")
            }

            checkboxId.setOnCheckedChangeListener { _, isChecked ->
                showToastMessage("Checkbox checked: $isChecked")
            }

            radioGroup.setOnCheckedChangeListener { _, i ->
                when (i) {
                    radioBtn1.id -> showToastMessage("Radio Button 1 selected")
                    radioBtn2.id -> showToastMessage("Radio Button 2 selected")
                    radioBtn3.id -> showToastMessage("Radio Button 3 selected")
                    radioBtn4.id -> showToastMessage("Radio Button 4 selected")
                }
            }
        }
    }

    private fun showToastMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }
}