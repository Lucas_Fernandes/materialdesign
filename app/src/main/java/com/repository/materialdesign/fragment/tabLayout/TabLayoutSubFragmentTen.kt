package com.repository.materialdesign.fragment.tabLayout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import com.repository.materialdesign.R
import com.repository.materialdesign.databinding.FragmentTabLayoutSubSixBinding
import com.repository.materialdesign.databinding.FragmentTabLayoutSubTenBinding

class TabLayoutFragmentTen : Fragment() {

    private lateinit var binding: FragmentTabLayoutSubTenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabLayoutSubTenBinding.inflate(layoutInflater)
        return binding.root
    }

}