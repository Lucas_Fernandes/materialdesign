package com.repository.materialdesign.fragment.bottomNavBar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.repository.materialdesign.databinding.FragmentBottomNavSubThreeBinding

class BottomNavSubFragmentThree : Fragment() {

    private lateinit var binding: FragmentBottomNavSubThreeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBottomNavSubThreeBinding.inflate(layoutInflater)
        return binding.root
    }

}