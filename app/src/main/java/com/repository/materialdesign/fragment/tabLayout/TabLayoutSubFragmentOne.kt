package com.repository.materialdesign.fragment.tabLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.repository.materialdesign.databinding.FragmentTabLayoutSubOneBinding

class TabLayoutSubFragmentOne : Fragment() {

    private lateinit var binding: FragmentTabLayoutSubOneBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabLayoutSubOneBinding.inflate(layoutInflater)
        return binding.root
    }
}