package com.repository.materialdesign.fragment.tabLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.repository.materialdesign.R
import com.repository.materialdesign.adapter.viewPager.ViewPagerAdapter
import com.repository.materialdesign.databinding.FragmentTabLayoutBinding

class TabLayoutFragment : Fragment() {

    private lateinit var binding: FragmentTabLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabLayoutBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            viewPager.adapter = ViewPagerAdapter(this@TabLayoutFragment)
            TabLayoutMediator(tabLayoutId, viewPager) { tab, position ->
                when (position) {
                    0 -> tab.text = getString(R.string.first_tab_item_text)
                    1 -> tab.text = getString(R.string.second_tab_item_text)
                    2 -> tab.text = getString(R.string.third_tab_item_text)
                    3 -> tab.text = getString(R.string.fourth_tab_item_text)
                    4 -> tab.text = getString(R.string.fifth_tab_item_text)
                    5 -> tab.text = getString(R.string.sixth_tab_item_text)
                    6 -> tab.text = getString(R.string.seventh_tab_item_text)
                    7 -> tab.text = getString(R.string.eighth_tab_item_text)
                    8 -> tab.text = getString(R.string.ninth_tab_item_text)
                    9 -> tab.text = getString(R.string.tenth_tab_item_text)
                }
            }.attach()
        }
    }
}