package com.repository.materialdesign.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.material.progressindicator.LinearProgressIndicator
import com.google.android.material.snackbar.Snackbar
import com.repository.materialdesign.R
import com.repository.materialdesign.databinding.FragmentMainBinding
import kotlinx.coroutines.*


class MainFragment : Fragment() {

    private var binding: FragmentMainBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addListeners()
    }

    private fun addListeners() {
        addTextFieldListeners()
        addButtonListeners()
    }

    private fun addButtonListeners() {
        binding?.apply {
            raisedBtn.setOnClickListener { showToastMessage("Raised button clicked!") }
            flatBtn.setOnClickListener { showToastMessage("Flat button was clicked!") }
            fabBtn.setOnClickListener { showSnackBar(this) }
            btnGoToSelectionControls.setOnClickListener { goToSelectionControls(it) }
            btnLinearProgressBar.setOnClickListener { showLinearProgressBar(this) }
            btnCircularProgressBar.setOnClickListener {
                showCircularProgressBar(this)
            }
        }
    }

    private fun goToSelectionControls(it: View) {
        Navigation.findNavController(it)
            .navigate(R.id.action_mainFragment_to_selectionControlsFragment)
    }

    private fun showCircularProgressBar(fragmentMainBinding: FragmentMainBinding) {
        fragmentMainBinding.apply {
            GlobalScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main) {
                    mainContent.visibility = View.GONE
                    progressbarCircular.let {
                        it.visibility = View.VISIBLE
                        doSomeDelayingCall()
                        it.visibility = View.GONE
                    }
                    mainContent.visibility = View.VISIBLE
                }
            }
        }
    }

    private suspend fun doSomeDelayingCall() {
        delay(1500L)
    }

    private fun showLinearProgressBar(fragmentMainBinding: FragmentMainBinding) {
        fragmentMainBinding.apply {
            GlobalScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main) {
                    mainContent.visibility = View.GONE
                    binding?.progressLinear?.let {
                        it.visibility = View.VISIBLE
                        it.max = 100
                        doAnotherDelayingCall(it)
                        it.visibility = View.GONE
                    }
                    mainContent.visibility = View.VISIBLE
                }
            }
        }
    }

    private suspend fun doAnotherDelayingCall(linearProgressIndicator: LinearProgressIndicator) {
        var counter = 0
        withContext(Dispatchers.IO) {
            for (i in 0..100) {
                delay(10)
                activity?.runOnUiThread {
                    linearProgressIndicator.progress = counter++
                }
            }
        }
    }

    private fun addTextFieldListeners() {
        binding?.run {
            usernameTextField.apply {
                addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable) {}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (text.toString().isEmpty()) {
                            usernameTextInputLayout.isErrorEnabled = true
                            usernameTextInputLayout.error = "Please enter your username!"
                        } else {
                            usernameTextInputLayout.isErrorEnabled = false
                        }
                    }
                })

                setOnFocusChangeListener { _, hasFocus ->
                    if (text.toString().isEmpty() && hasFocus) {
                        usernameTextInputLayout.isErrorEnabled = true
                        usernameTextInputLayout.error = "Please enter your username!"
                    } else {
                        usernameTextInputLayout.isErrorEnabled = false
                    }
                }
            }

            passwordTextInputLayout.apply {
                isCounterEnabled = true
                counterMaxLength = 8
            }
        }
    }

    private fun showToastMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    private fun showSnackBar(fragmentMainBinding: FragmentMainBinding) {
        activity?.let {
            Snackbar.make(
                it,
                fragmentMainBinding.mainFragmentLayout,
                getString(R.string.snack_bar_message),
                Snackbar.LENGTH_SHORT
            ).run {
                duration = 2000
                setAction("Dismiss") { dismiss() }
                anchorView = fragmentMainBinding.fabBtn
                setActionTextColor(
                    ContextCompat.getColor(
                        it,
                        R.color.purple_A200
                    )
                )
                setTextColor(
                    ContextCompat.getColor(
                        it,
                        R.color.yellow_50_A700
                    )
                )
                setBackgroundTint(
                    ContextCompat.getColor(
                        it,
                        R.color.light_blue_900
                    )
                )
                show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}