package com.repository.materialdesign.fragment.tabLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.repository.materialdesign.databinding.FragmentTabLayoutSubNineBinding

class TabLayoutFragmentNine : Fragment() {

    private lateinit var binding: FragmentTabLayoutSubNineBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabLayoutSubNineBinding.inflate(layoutInflater)
        return binding.root
    }

}