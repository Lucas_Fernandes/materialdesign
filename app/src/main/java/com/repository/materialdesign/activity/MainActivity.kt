package com.repository.materialdesign.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.repository.materialdesign.R
import com.repository.materialdesign.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    private lateinit var appBarConfig: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        navController = (
            supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        ).navController

        appBarConfig = AppBarConfiguration(navController.graph, binding.myDrawerLayout)

        setContentView(binding.root)
        setSupportActionBar(binding.mainToolbar.myToolbar)
        addDrawerListener()
        setUpNavigation()
        setUpBottomNavBadges()
        addBottomNavViewListener()
        addNavControllerListener()
    }

    private fun addNavControllerListener() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.mainFragment -> {
                    binding.bottomNavView.visibility = View.GONE
                    unlockDrawer()
                }
                R.id.bottomNavSubFragmentPageOne -> {
                    binding.bottomNavView.visibility = View.VISIBLE
                    lockDrawer()
                }
                R.id.bottomNavSubFragmentPageTwo -> {
                    binding.bottomNavView.visibility = View.VISIBLE
                    lockDrawer()
                }
                R.id.bottomNavSubFragmentPageThree -> {
                    binding.bottomNavView.visibility =
                        View.VISIBLE
                }
                R.id.selectionControlsFragment -> {
                    binding.bottomNavView.visibility = View.GONE
                    lockDrawer()
                }

                R.id.tabLayoutFragment -> lockDrawer()
            }
        }
    }

    private fun addBottomNavViewListener() {
        binding.bottomNavView.apply {
            setOnNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.bottomNavSubFragmentPageOne -> {
                        navController.navigate(R.id.bottomNavSubFragmentPageOne)
                        true
                    }
                    R.id.bottomNavSubFragmentPageTwo -> {
                        navController.navigate(R.id.bottomNavSubFragmentPageTwo)
                        true
                    }
                    R.id.bottomNavSubFragmentPageThree -> {
                        navController.navigate(R.id.bottomNavSubFragmentPageThree)
                        true
                    }
                    else -> {
                        navController.navigate(R.id.bottomNavSubFragmentPageOne)
                        false
                    }
                }
            }

            setupWithNavController(navController)
        }
    }

    private fun setUpBottomNavBadges() {
        binding.bottomNavView.let {
            it.getOrCreateBadge(R.id.bottomNavSubFragmentPageOne).run {
                isVisible = true
            }
            it.getOrCreateBadge(R.id.bottomNavSubFragmentPageTwo).run {
                isVisible = true
                number = 99
            }
            it.getOrCreateBadge(R.id.bottomNavSubFragmentPageThree).run {
                isVisible = true
                number = 1500
            }
        }
    }

    private fun setUpNavigation() {
        setupActionBarWithNavController(
            navController,
            appBarConfig
        )
    }

    override fun onBackPressed() {
        binding.myDrawerLayout.run {
            if (isDrawerOpen(GravityCompat.START)) {
                closeDrawer(GravityCompat.START)
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search_id -> showToastMessage("Search icon was touched")
            R.id.cart_id -> showToastMessage("Cart icon was touched")
            R.id.alert_dialog_menu_id -> displayAlertDialog()
            R.id.confirmation_dialog_menu_id -> showConfirmationDialog()
            R.id.bottom_nav_menu_id -> handleBottomNavigation()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp(appBarConfig)

    private fun handleBottomNavigation() {
        when (navController.currentDestination?.id) {
            R.id.selectionControlsFragment -> navController.navigate(
                R.id.action_selectionControlsFragment_to_bottomNavSubFragmentPageOne
            )

            R.id.mainFragment -> navController.navigate(
                R.id.action_mainFragment_to_bottomNavSubFragmentPageOne
            )
        }
    }

    private fun showConfirmationDialog() {
        val singleItems = arrayOf("Item 1", "Item 2", "Item 3")

        MaterialAlertDialogBuilder(
            this@MainActivity,
            R.style.ConfirmationDialogTheme
        )
            .setTitle("Confirmation Dialog")
            .setNeutralButton("Neutral Button (Cancel)") { dialog, _ -> dialog.dismiss() }
            .setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }
            .setSingleChoiceItems(singleItems, -1) { _, i ->
                showToastMessage("Item ${singleItems[i]} was selected")
            }
            .show()
    }

    private fun displayAlertDialog() {
        MaterialAlertDialogBuilder(
            this@MainActivity,
            R.style.AlertDialogTheme
        )
            .setTitle("Title")
            .setMessage("This is an alert dialog!")
            .setNegativeButton("Dismiss") { dialog, _ ->
                showToastMessage("Dismiss option was clicked")
                dialog.dismiss()
            }
            .setPositiveButton("ok") { dialog, _ ->
                showToastMessage("Ok option was clicked")
                dialog.dismiss()
            }
            .setNeutralButton("Neutral button") { dialog, _ ->
                showToastMessage("Neutral option was clicked")
                dialog.dismiss()
            }
            .show()
    }

    private fun showToastMessage(message: String) {
        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun addDrawerListener() {
        binding.let {
            it.myNavigationView.setNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.inbox_id -> {
                        navController.navigate(R.id.action_mainFragment_to_tabLayoutFragment)
                        lockDrawer()
                        closeDrawer()
                        true
                    }
                    R.id.starred_id -> {
                        showToastMessage("Starred")
                        closeDrawer()
                        true
                    }
                    R.id.sent_id -> {
                        showToastMessage("Sent")
                        closeDrawer()
                        true
                    }
                    R.id.drafts_id -> {
                        showToastMessage("Drafts")
                        closeDrawer()
                        true
                    }
                    R.id.all_mail_id -> {
                        showToastMessage("All mail")
                        closeDrawer()
                        true
                    }
                    R.id.trash_id -> {
                        showToastMessage("Trash")
                        closeDrawer()
                        true
                    }
                    R.id.spam_id -> {
                        showToastMessage("Spam")
                        closeDrawer()
                        true
                    }

                    else -> false
                }
            }
        }
    }

    private fun lockDrawer() {
        binding.myDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    private fun unlockDrawer() {
        binding.myDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    private fun closeDrawer() {
        binding.myDrawerLayout.closeDrawer(GravityCompat.START)
    }
}